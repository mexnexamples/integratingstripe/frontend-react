# React app frontend for Stripe integration

The corresponding backend can be found [here](https://gitlab.com/mexnexamples/integratingstripe/backend)

## Steps to use
* After downloading, create a .env file within the frontend folder
* Then enter the REACT_APP_BACKENDURL and REACT_APP_PUBLISHABLE_STRIPE_KEY variables
* Use npm install
* Fire up the backend, then use npm start to fire up this frontend

## Examples of .env variables
* REACT_APP_BACKENDURL="http://localhost:8000/"
* REACT_APP_PUBLISHABLE_STRIPE_KEY="pk_test_4S689YmLDvfASO91mS8jUFT9"