import './App.css';
import { useEffect, useState } from 'react';
import axios from 'axios';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import Choose from "./components/Choose";
import CheckoutForm from "./components/CheckoutForm";
import LoginOrRegister from './components/LoginOrRegister';

// Calls loadStripe outside of a component’s render to avoid
// recreating the Stripe object on every render.
const promise = loadStripe(process.env.REACT_APP_PUBLISHABLE_STRIPE_KEY);             // This is an API key from the dotenv file, replace it with your own publishable API key

function App() {

  const [items, setItems] = useState([]);
  const [numOfItems, setNumOfItems] = useState(0);
  const [checkingOut, setCheckingOut] = useState(false);
  const [loggedIn, setLoggedIn] = useState(false);                          // Replace this with the login system of your choice
  const [user, setUser] = useState({UserName: "", Password: ""});           // Replace this with the user system of your choice

  const setDefaultAmountForItems = async (itemsToHandle) => {
    let itemsWithDefaultAmount = itemsToHandle.map(
      item => ({ ...item, Amount: 0 })
    );
    setItems(itemsWithDefaultAmount);
  };

  useEffect(() => {
    axios.get(process.env.REACT_APP_BACKENDURL + "api/allitems")
    .then((res) => setDefaultAmountForItems(res.data));
  }, []);

  useEffect(() => {
    checkingOut && setDefaultAmountForItems(items);
  }, [checkingOut]);

  return (
    <div className="App mt-2">
      { checkingOut && (
        <Elements stripe={promise} >
          <CheckoutForm 
            items={items} 
            user={user} 
            setCheckingOut={setCheckingOut} 
          />
        </Elements>
      )}
      { !checkingOut && (
        <Container>
          { !loggedIn && (
            <Row>
              <LoginOrRegister 
                user={user} 
                setUser={setUser} 
                setLoggedIn={setLoggedIn} 
              />
            </Row>
          )}
          { loggedIn && (
            <Row>
              <Col style={{ marginBottom: "20px" }}>
                <Choose 
                  items={items} 
                  setItems={setItems} 
                  numOfItems={numOfItems} 
                  setNumOfItems={setNumOfItems} 
                />
                <Button 
                  className="mt-4 w-25" 
                  onClick={() => setCheckingOut(true)} 
                  disabled={numOfItems === 0} 
                >
                  Check out
                </Button>
              </Col>
            </Row>
          )}
        </Container>
      )}
    </div>
  );
}

export default App;
