import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';


export default function Choose ({ items, setItems, numOfItems, setNumOfItems }) {

    const increaseAmount = (item) => {
        let indexOfItem = items.findIndex(
            (itemInArray) => itemInArray === item
        );
        let alteredItems = items.slice();
        alteredItems[indexOfItem] = {
            ...item, Amount: item.Amount + 1
        };
        setItems(alteredItems);
        setNumOfItems(numOfItems + 1);
    };

    const decreaseAmount = (item) => {
        let indexOfItem = items.findIndex(
            (itemInArray) => itemInArray === item
        );
        let alteredItems = items.slice();
        alteredItems[indexOfItem] = {
            ...item, Amount: (item.Amount === 0) ? (item.Amount) : (item.Amount - 1)
        };
        setItems(alteredItems);
        numOfItems === 0 ? console.log("") : setNumOfItems(numOfItems - 1);
    };

    return (
        <Container fluid="true" >
            <Row 
                xs={1} 
                sm={3} 
                md={6} 
                className="g-4 d-flex justify-content-center"
            >
                { items.map((item) => (
                    <Col key={item._id}>
                        <Card>
                            <Card.Img 
                                variant="top" 
                                src={item.Picture} 
                                alt="Item to buy"
                            />
                            <Card.Body>
                                <Card.Title className="h5">
                                    {item.Name}
                                </Card.Title>
                                <Row 
                                    className="d-flex justify-content-center"
                                >
                                    <div 
                                        className="col-2 h5" 
                                        style={{cursor: "pointer"}} 
                                        onClick={() => increaseAmount(item)}
                                    >
                                        +
                                    </div>
                                    <Card.Text className="col-4 pl-2 pr-2">
                                        {item.Amount}
                                    </Card.Text>
                                    <div 
                                        className="col-2 h5" 
                                        style={{cursor: "pointer"}} 
                                        onClick={() => decreaseAmount(item)}
                                    >
                                        -
                                    </div>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                )) }
            </Row>
        </Container>
    );
};