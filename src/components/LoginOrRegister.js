import axios from "axios";
import { useState } from "react";

export default function LoginOrRegister ({user, setUser, setLoggedIn}) {
    
    const [errorMessage, setErrorMessage ] = useState("");

    const tryLogin = (e) => {
        e.preventDefault();
        const loginUrl = process.env.REACT_APP_BACKENDURL + "loginorregister";
        axios.post(loginUrl, user)
            .then(res => {
                res.status === 200 
                ? setLoggedIn(true) 
                : setErrorMessage(res.data.errorMessage)
            })
            .catch(e => console.log(e));
    };

    return (
        <form>
            <div>
                <input 
                    type="text" 
                    onChange={(e) => setUser({...user, UserName: e.target.value})} 
                />
                <input 
                    type="password" 
                    onChange={(e) => setUser({...user, Password: e.target.value})} 
                />
                { errorMessage }
            </div>
            <div>
                <button onClick={(e) => tryLogin(e)}>
                    Log In / Register
                </button>
            </div>
        </form> 
    );
};